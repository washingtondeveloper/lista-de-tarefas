import React from 'react';
import { shallow } from 'enzyme';
import Grid from './Grid';

describe('Grid Component', () => {
	describe('Grid function fromNumberToClass', () => {
		let grid;

		beforeEach(() => {
			grid = new Grid();
		});

		it('Should return class col-12', () => {
			const expectClass = grid.fromNumberToClass('12');

			expect(expectClass).toEqual('col-12');
		});

		it('Should return class col-12 col-sm-6', () => {
			const expectClass = grid.fromNumberToClass('12 6');

			expect(expectClass).toEqual('col-12 col-sm-6');
		});

		it('Should return class col-12 col-sm-2 col-md-8', () => {
			const expectClass = grid.fromNumberToClass('12 2 8');

			expect(expectClass).toEqual('col-12 col-sm-2 col-md-8');
		});

		it('Should return class col-12 col-sm-11 col-md-3 col-lg-5', () => {
			const expectClass = grid.fromNumberToClass('12 11 3 5');

			expect(expectClass).toEqual('col-12 col-sm-11 col-md-3 col-lg-5');
		});
	});

	describe('Grid Component render', () => {
		it('Should add class col-12', () => {
			const component = shallow(<Grid />);
			const wrapper = component.hasClass('col-12');

			expect(wrapper).toBeTruthy();
		});

		it('Should add class col-sm-6', () => {
			const component = shallow(<Grid cols="12 6" />);
			const wrapper = component.hasClass('col-sm-6');

			expect(wrapper).toBeTruthy();
		});

		it('Should add class col-md-5', () => {
			const component = shallow(<Grid cols="12 6 5" />);
			const wrapper = component.hasClass('col-md-5');

			expect(wrapper).toBeTruthy();
		});

		it('Should add class col-lg-9', () => {
			const component = shallow(<Grid cols="12 6 5 9" />);
			const wrapper = component.hasClass('col-lg-9');

			expect(wrapper).toBeTruthy();
		});
	});
});
