import React from 'react'

import ListTasks from '../../../../listTasks/ListTasks'

export default props => {
    if(props.list.length > 0 ) 
        return <ListTasks list={props.list} /> 
    else 
        return(
            <div>
                <h4>{props.text}</h4>
            </div>
        )
}