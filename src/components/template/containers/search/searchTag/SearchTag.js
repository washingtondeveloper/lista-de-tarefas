import React, { Component } from 'react'
import { connect } from 'react-redux'

import Tags from '../../../../taskForm/tags/Tags'
import GeneratorList from '../generatorList/GeneratorList';

let showingTasks = []
let prewTag = {}

class SearchTag extends Component {

    hasTagInTask = (tasks, lastTag) => { 
        return tasks.filter(task => task.tags.some(tg =>  tg.text === lastTag.text) && task)
    }

    render() {

        const { tags, tasks } = this.props

        if (tags.length > 0) {
            const lastTag = tags[tags.length - 1]
            
            /**Esse if vai funcionar somete na primeira vez */
            if(!prewTag.id) {
                prewTag = lastTag
                showingTasks = [...showingTasks, ...this.hasTagInTask(tasks, lastTag)]
            }
            /* Não adiciona a mesma task */
            if( prewTag.id !== lastTag.id)
                showingTasks = [...showingTasks, ...this.hasTagInTask(tasks, lastTag)]
        }

        if(tags.length === 0)
            showingTasks = []
        return (
            <div>
                <h2>Pesquisa por Tag</h2>
                <Tags />
                <GeneratorList text="Adicione uma tag para verificar." list={showingTasks} />
            </div>
        )
    }
}

const mapStateToProps = state => ({ tags: state.tasks.task.tags, tasks: state.tasks.list })
export default connect(mapStateToProps)(SearchTag)