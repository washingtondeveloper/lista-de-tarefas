import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import scapeRegExp from 'escape-string-regexp'
import sortBy from 'sort-by'

import { allTasks } from '../../../../../store/actions/tasks/tasksAction'
import GeneratorList from '../generatorList/GeneratorList'

class SearchTask extends Component {

    state = {
        search: ''
    }

    updateSearch = search => {
        this.setState({ search: search.trim() })
    }

    render() {

        const { list } = this.props
        const { search } = this.state

        let showingTasks

        if (search) {
            const match = new RegExp(scapeRegExp(search), 'i')
            showingTasks = list.filter(task => match.test(task.description))
        } else {
            showingTasks = list
        }

        showingTasks.sort(sortBy('description'))

        return (
            <div>
                <h2>Pesquisas</h2>
                <input
                    style={{ marginBottom: '15px' }}
                    className="form-control form-control-lg"
                    type="text"
                    value={this.state.search}
                    onChange={e => this.updateSearch(e.target.value)}
                    placeholder="Lembra da descrição?..." />

                <GeneratorList text="Ops... nenhuma encontrada" list={showingTasks} />

            </div>
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list })
const mapDispatchToProps = dispatch => bindActionCreators({ allTasks }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(SearchTask)