import React, { Component } from 'react'
import { connect } from 'react-redux'

import WindowList from '../windowList/WindowList'
import Utils from '../../../../../Utils/Utils';

class TasksMonth extends Component {

    render() {

        const tasksMonth = this.props.list.filter(task => Utils.isTasksOfMonth(task))

        return (
            <WindowList text="Não à tarefa para esse Mês" title="Tarefa do Mês" list={tasksMonth} />
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list })

export default connect(mapStateToProps)(TasksMonth)