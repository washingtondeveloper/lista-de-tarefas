import React, { Component } from 'react'
import { connect } from 'react-redux'
import Utils from '../../../../../Utils/Utils';
import WindowList from '../windowList/WindowList';

class TasksToday extends Component {

    constructor(props) {
        super(props)

        this.dateNow = new Date()
    }


    render() {

        const listToday = this.props.list.filter(task => Utils.isTaskToday(task))

        return (
            <WindowList list={listToday} text="Parabéns sinal que esta tudo bem." title="Tarefas de Hoje" />
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list })

export default connect(mapStateToProps)(TasksToday)