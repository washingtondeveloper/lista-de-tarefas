import React, { Component } from 'react'
import { Tabs, Tab } from 'react-bootstrap'

import './Search.scss'
import ContentHeader from '../../../contentHeader/ContentHeader'
import Performed from './performed/Performed'
import NotPerformed from './notPerformed/NotPerformed'
import SearchTask from './searchTask/SearchTask'
import TasksToday from './tasksToday/TasksToday'
import TasksMonth from './tasksMonth/TasksMonth';
import SearchTag from './searchTag/SearchTag';

class Search extends Component {

    render() {
        return (
            <div>
                <ContentHeader title="O que pesquisar?" small="Pesquisar" />
                <Tabs defaultActiveKey="performed" id="uncontrolled-tab-example">
                    <Tab eventKey="performed" title="Realizadas">
                        <Performed />
                    </Tab>
                    <Tab eventKey="notPerformed" title="Não Realizadas">
                        <NotPerformed />
                    </Tab>
                    <Tab eventKey="seachTask" title="Pesquisa">
                        <SearchTask />
                    </Tab>
                    <Tab eventKey="searchTag" title="Pesquisa por Tag">
                        <SearchTag />
                    </Tab>
                    <Tab eventKey="tasksToday" title="Tarefas de Hoje">
                        <TasksToday />
                    </Tab>
                    <Tab eventKey="tasksMonth" title="Tarefas do Mês">
                        <TasksMonth />
                    </Tab>
                    
                </Tabs>
            </div>
        )
    }
}

export default Search