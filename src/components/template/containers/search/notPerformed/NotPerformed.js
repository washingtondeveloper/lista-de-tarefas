import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { allTasks } from '../../../../../store/actions/tasks/tasksAction'
import WindowList from '../windowList/WindowList';


class NotPerformed extends Component {

    componentDidMount = () => {
        this.props.allTasks()
    }

    render() {

        const notPerformed = this.props.list.filter(task => !task.done)

        return (
            <WindowList list={notPerformed} text="Parabéns você esta em dia." title="Não Realizadas" />
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list })
const mapDispatchToProps = dispatch => bindActionCreators({ allTasks }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(NotPerformed)