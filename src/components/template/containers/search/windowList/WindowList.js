import React from 'react'
import GeneratorList from '../generatorList/GeneratorList';

export default props => (
    <div>
        <h2>{props.title}</h2>
        <GeneratorList text={props.text} list={props.list} />
    </div >
)