import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { allTasks } from '../../../../../store/actions/tasks/tasksAction'
import WindowList from '../windowList/WindowList';

class Performed extends Component {

    componentDidMount = () => {
        this.props.allTasks()
    }

    render() {

        const performeds = this.props.list.filter(task => task.done)

        return (
            <WindowList list={performeds} text="Puxa... você não tem nenhuma concluida :-(" title="Realizadas" />
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list })
const mapDispatchToProps = dispatch => bindActionCreators({ allTasks }, dispatch)
export default connect(mapStateToProps, mapDispatchToProps)(Performed)