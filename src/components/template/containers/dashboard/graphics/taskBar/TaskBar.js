import React, { Component } from 'react'
import {Chart} from 'primereact/chart'

class TaskBar extends Component {

    render () {
        const data = {
            labels: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho'],
            datasets: [
                {
                    label: 'Tarefas Realizadas',
                    backgroundColor: '#42A5F5',
                    data: [65, 59, 80, 81, 56, 55, 40]
                },
                {
                    label: 'Não Realizadas',
                    backgroundColor: '#9CCC65',
                    data: [28, 48, 40, 19, 86, 27, 90]
                }
            ]    
        }

        return (
            <div>
                <div className="content-section introduction">
                    <div className="feature-intro">
                        <h2>Minhas Tarefas</h2>
                        <p>Mais um motivo de você deixar suas atividades em dia <i className="fa fa-smile-o" aria-hidden="true"></i></p>
                    </div>
                </div>

                <div className="content-section implementation">
                    <Chart type="bar" data={data} />
                </div>
            </div>
        )
    }
}

export default TaskBar