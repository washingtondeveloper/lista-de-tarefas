import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button } from 'react-bootstrap';

import ContentHeader from '../../../contentHeader/ContentHeader';
import Grid from '../../../grid/Grid';
import { allTasks } from '../../../../store/actions/tasks/tasksAction'
import * as actionsTags from '../../../../store/actions/tags/tagsAction'
import * as actionsTemplate from '../../../../store/actions/template/templateAction'
import BarButton from '../../../barButton/BarButton';

class ListTags extends Component {

    componentDidMount = () => {
        this.props.allTags()
        this.props.changeForm({
            type: 'tag',
            title: 'Criando uma Tag'
        })
    }

    render() {

        const tags = this.props.tags

        return (
            <div>
                <ContentHeader title="Tags" small="Lista de Tags" />
                <BarButton>
                    <Button variant="outline-primary" onClick={e => {
                        e.preventDefault()
                        this.props.showModal()
                    }}>
                        Tags
                    </Button>
                </BarButton> 
                <div className="row">
                    {tags.map((tag, i) => (
                        <Grid  key={i} cols="6 2">
                            <div className="badge badge-success m-2">
                                {tag.text}
                            </div>
                        </Grid>
                    ))}
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => ({ tags: state.tags.tags })
const mapDispatchToProps = dispatch => bindActionCreators({ allTasks, ...actionsTemplate, ...actionsTags }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(ListTags)