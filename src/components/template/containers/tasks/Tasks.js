import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Button } from 'react-bootstrap';

import ContentHeader from '../../../contentHeader/ContentHeader'
import BarButton from '../../../barButton/BarButton'
import * as actionsTasks from '../../../../store/actions/tasks/tasksAction'
import * as actionsTemplate from '../../../../store/actions/template/templateAction'
import ListTasks from '../../../listTasks/ListTasks'
import Utils from '../../../../Utils/Utils';

class Tasks extends Component {


    componentDidMount = () => {
        this.props.allTasks()
        this.props.changeForm({
            type: 'task',
            title: 'Criando uma Tarefa'
        })

        
        const { list } = this.props

        let descriptions = ''

        list.forEach(task => {
            if(Utils.isTaskToday(task) && !Utils.isTaskDone(task)) {
                descriptions += `Tarefa: ${task.description}\n`
            }
        })

        if(descriptions.length){

            new Notification('Você tem tarefas que vence Hoje', {
                body: descriptions,
            })
        }

    }

    render() {
    
        return (
            <div>
                <ContentHeader title="Tarefas" small="Lista de Tarefas" />
                <BarButton>
                    <Button variant="outline-primary" onClick={this.props.showModal}>
                        Adicionar
                    </Button>
                </BarButton>                
                
                <ListTasks list={this.props.list}/>

            </div>
        )
    }
}

const mapStateToProps = state => ({ list: state.tasks.list, modal: state.template.modal })
const mapDispatchToProps = dispatch => bindActionCreators({ ...actionsTasks, ...actionsTemplate }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Tasks)
