import React from 'react'

import './index.scss'
import Header from './header/Header'
import Content from './content/Content'
import Footer from './footer/Footer'
import Routers from '../../routers'
import Messages from '../messages/Messages'
import Modal from '../modal/Modal'

export default () => (
    <main className="main">
        <Header />
        <Content>
            <Routers />
        </Content>
        <Footer />
        <Messages />
        <Modal />
    </main>
)