import React from 'react'

import './Footer.scss'

export default () => (
    <footer className="t-footer bg-dark">
        <div>
            <span className="t-footer-description">Copyright &copy; {new Date().getFullYear()}</span> <a target="_was" href="https://washingtondeveloper.com.br">Washington Developer</a>
        </div>
    </footer>
)