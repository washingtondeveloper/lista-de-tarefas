import React from 'react'
import { shallow } from 'enzyme'

import Footer from './Footer'

describe('Footer Component', () => {

    let component

    beforeEach(() => {
        component = shallow(<Footer />)
    })

    it('Should render without erros', () => {
        const wrapper = component.find('.t-footer')
        expect(wrapper.length).toBe(1)
    })

    it('Should found a span with the class t-footer-description', () => {
        const wrapper = component.find('.t-footer-description')
        expect(wrapper.length).toBe(1)
    })

    it('Should found a link with description Washington Developer', () => {
        const wrapper = component.find('a[target="_was"]').text()
        expect(wrapper.toString()).toEqual('Washington Developer')
    })
})