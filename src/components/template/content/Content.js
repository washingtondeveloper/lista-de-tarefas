import React from 'react'

import './Content.scss'

export default props => (
    <article className="t-content container">
        {props.children}
    </article>
)