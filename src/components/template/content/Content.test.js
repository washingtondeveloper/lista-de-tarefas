import React from 'react'
import { shallow } from 'enzyme'

import Content from './Content'

describe('Content Component', () => {

    let component

    beforeEach(() => {
        component = shallow(<Content />)
    })

    it('Should render without erros', () => {
        const wrapper = component.find('.container')
        expect(wrapper.length).toBe(1)
    })
})