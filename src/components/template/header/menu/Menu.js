import React from 'react'

export default props => (
    <div className={props.auto ? 'mr-auto navbar-nav' : 'navbar-nav'}>
        {props.children}
    </div>
)