import React from 'react'
import { shallow } from 'enzyme'

import Menu from './Menu'

describe('Menu Component', () => {

    it('Should add only class navbar-nav', () => {
        const componet = shallow(<Menu />)
        const wrapper = componet.find('div.navbar-nav')
        expect(wrapper.find('.mr-auto').length).toBe(0)
        expect(wrapper.length).toBe(1)
    })

    it('Should add class "mr-auto" and "navbar-nav"', () => {
        const componet = shallow(<Menu auto/>)
        const wrapper = componet.find('div.mr-auto')
        expect(wrapper.find('div.navbar-nav').length).toBe(1)
        expect(wrapper.length).toBe(1)
    })
})