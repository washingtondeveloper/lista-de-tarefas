import React from 'react'
import { shallow } from 'enzyme'

import Header from './Header'

const setUp = (props={}) => {
    const component = shallow(<Header {...props}/>)
    return component
}

describe('Header Component', () => {

    let component

    beforeEach(() => {
        component = setUp()
    })

    it('Should render without erros', () => {
        const wrapper = component.find('.t-header')
        expect(wrapper.length).toBe(1)
    })

    it('Should render a logo', () => {
        const wrapper = component.find('img[src="task.png"]')
        expect(wrapper.length).toBe(1)
    })
})