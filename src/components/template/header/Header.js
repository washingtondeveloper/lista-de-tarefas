import React from 'react'
import { Navbar, Container } from 'react-bootstrap'

import img_task from '../../../assets/img/task.png'
import './Header'
import Menu from './menu/Menu'
import ItemMenu from './itemMenu/ItemMenu'

export default () => (
    <Navbar className="t-header" collapseOnSelect expand="lg" bg="dark" variant="dark">
        <Container>
            <Navbar.Brand>
                <img
                    alt=""
                    src={img_task}
                    width="30"
                    height="30"
                    className="d-inline-block align-top"
                />
                {' Tarefas'}
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Menu auto>
                    <ItemMenu to="/" icon="tachometer" description="Dashboard"/>
                    <ItemMenu to="/tasks" icon="tasks" description="Lista de Tarefas"/>
                    <ItemMenu to="/tags" icon="tag" description="Lista de Tags"/>
                </Menu>
                <Menu>
                    <ItemMenu to="/search" icon="search" description="Pesquisar"/>
                </Menu>
            </Navbar.Collapse>
        </Container>
    </Navbar>
)