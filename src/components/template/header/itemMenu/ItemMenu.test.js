import React from 'react'
import { shallow } from 'enzyme'

import ItemMenu from './ItemMenu'

describe('ItemMenu Component', () => {

    it('Should render without erros', () => {
        const component = shallow(<ItemMenu to="/teste" description="desc_teste"/>)
        const wrapper = component.find('.nav-link')

        expect(wrapper.length).toBe(1)
    })
})