import React from 'react'
import { NavLink } from 'react-router-dom'

export default props => (
    <NavLink exact to={props.to} className="nav-link">
        <i className={`fa fa-${props.icon}`} aria-hidden="true"></i> {props.description}
    </NavLink>
)