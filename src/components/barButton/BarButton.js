import React from 'react';

import './BarButton.scss';

export default (props) => <div className="t-bar-button">{props.children}</div>;
