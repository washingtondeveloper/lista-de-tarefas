import React, { Component } from 'react';
import { WithContext as ReactTags } from 'react-tag-input';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import { addTag, removeTag, dragTag } from '../../../store/actions/tags/tagsAction';

const KeyCodes = {
	comma: 188,
	enter: 13
};

const delimiters = [ KeyCodes.comma, KeyCodes.enter ];

class Tags extends Component {
	render() {
		const { tags } = this.props;

		console.log(tags);

		return (
			<ReactTags
				tags={tags}
				classNames={{
					tagInputField: 'form-control mt-2',
					selected: 'pb-1 pt-1',
					tag: 'btn btn-primary tags-width tag-margin',
					remove: 'close'
				}}
				placeholder="Tags..."
				name="tags"
				handleDelete={this.props.removeTag}
				handleAddition={this.props.addTag}
				handleDrag={this.props.dragTag}
				delimiters={delimiters}
			/>
		);
	}
}

const mapStateToProps = (state) => ({ tags: state.tags.tags });
const mapDispatchToProps = (dispatch) => bindActionCreators({ addTag, removeTag, dragTag }, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Tags);
