import React from 'react'

export default props => (
    <div className="form-check form-check-inline">
        <input name={props.name} className="form-check-input" type="radio" value={props.value} checked={props.checked} onChange={props.onChange} />
        <label className="form-check-label" htmlFor={props.name}>{props.label}</label>
    </div>
)