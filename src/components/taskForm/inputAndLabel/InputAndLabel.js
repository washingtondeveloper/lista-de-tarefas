import React from 'react'

import Grid from '../../grid/Grid'

export default props => (
    <Grid cols={props.cols}>
        <div className="form-group">
            <label htmlFor={props.name}>{props.label}</label>
            <input name={props.name} 
                onChange={props.onChange}
                type={props.type} 
                value={props.value} 
                className="form-control" placeholder={props.placeholder} />
        </div>
    </Grid>
)