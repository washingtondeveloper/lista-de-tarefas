import React from 'react'
import { Calendar } from 'primereact/calendar'

import Grid from '../../grid/Grid'

export default props => (
    <Grid cols={props.cols}>
        <div className="form-group">
            <label htmlFor={props.name}>{props.label}</label>
            <Calendar name={props.name} 
                value={props.value} 
                onChange={e => props.onChange(e)} 
                timeOnly={true} hourFormat="24"/>
        </div>
    </Grid>
)