import React from 'react'
import { Calendar } from 'primereact/calendar'

import Grid from '../../grid/Grid'

export default props => (
    <Grid cols={props.cols}>
        <div className="form-group">
            <label htmlFor={props.name}>{props.label}</label>
            <Calendar dateFormat="dd/mm/yy" disabledDays={[0,6]} readOnlyInput={props.readOnly} name={props.name} value={props.value} onChange={e => props.onChange(e)} showTime={true} showSeconds={true} />
        </div>
    </Grid>

)