import React, { Component } from 'react'
import { Button } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import InputAndLabel from './inputAndLabel/InputAndLabel'
import RadioAndLabel from './radioAndLabel/RadioAndLabel'
import BarButton from '../barButton/BarButton'
import InputDate from './inputDate/InputDate'
import InputTime from './inputTime/InputTime'
import * as actionsTask from '../../store/actions/tasks/tasksAction'
import * as actionsTemplate from '../../store/actions/template/templateAction'
import * as actionsTags from '../../store/actions/tags/tagsAction'

class TaskForm extends Component {

    componentDidMount = () => {
        this.props.allTags()
    }

    render() {

        return (
            <form onSubmit={this.props.handleSubmit}>
                <input name="id" type="hidden" value={this.props.id} />

                <div className="form-row">
                    <InputAndLabel cols="12 12" name="description"
                        label="Descrição" value={this.props.description}
                        onChange={e => this.props.changeDescription(e.value)}
                        placeholder="Descrição..." />
                </div>

                <div className="form-row">

                    <InputTime name="forecastTaskDuration"
                        onChange={e => this.props.changeForecastTaskDuration(e.value)}
                        value={this.props.forecastTaskDuration}
                        label="Previsão para duração da tarefa" cols="12 4" />

                    <InputDate name="dateTimeWillMake"
                        readOnly={false}
                        onChange={(e) => this.props.changeDateTimeWillMake(e.value)}
                        value={this.props.dateTimeWillMake} label="Data e hora que a tarefa acontecerá"
                        cols="12 6" />
                </div>

                <div className="form-row">

                    <InputTime name="timeTaskReminder"
                        onChange={(e) => this.props.changeTimeTaskReminder(e.value)}
                        value={this.props.timeTaskReminder}
                        label="Tempo para lembrete da tarefa" cols="12 4" />

                    <InputDate name="createdAt"
                        readOnly={true}
                        onChange={(e) => this.props.changeCreatedAt(e.value)}
                        value={this.props.createdAt} label="Data e hora da criação da tarefa"
                        cols="12 6" />
                </div>

                <div className="form-row">
                    <div className="form-check form-check-inline">
                        <label className="form-check-label" htmlFor="inlineRadio1">Tarefa finalizada?</label>
                    </div>
                    <RadioAndLabel name="done" label="SIM" value="true" checked={JSON.parse(this.props.done) === true} onChange={e => this.props.changeDone(e.target.value)} />
                    <RadioAndLabel name="done" label="NÃO" value="false" checked={JSON.parse(this.props.done) === false} onChange={e => this.props.changeDone(e.target.value)} />
                </div>
                <div className="form-now">
                    <div className="form-group">
                        <h4>Tags</h4>
                        {this.props.tags.map((tag, i) => (
                            <div key={i} className="form-check form-check-inline">
                                <input className="form-check-input" value={JSON.stringify(tag)} checked={this.props.task.tags.some(t => t.id === tag.id)} type="checkbox" id="inlineCheckbox1" onChange={e => {
                                    if(e.target.checked)
                                        this.props.addTagTask(JSON.parse(e.target.value))
                                    else
                                        this.props.removeTagTask(JSON.parse(e.target.value))
                                }} />
                                <label className="form-check-label" htmlFor="inlineCheckbox1">{tag.text}</label>
                            </div>
                        ))}
                    </div>
                </div>
                <BarButton>
                    <Button variant="outline-danger" onClick={() => {
                        this.props.hideModal()
                        this.props.cleanForm()
                    }}>Cancelar</Button> {' '}
                    <Button variant="outline-success" type="submit">Adicionar</Button>
                </BarButton>
            </form>
        )
    }
}
const mapStateToProps = state => ({
    description: state.tasks.task.description,
    forecastTaskDuration: state.tasks.task.forecastTaskDuration,
    dateTimeWillMake: state.tasks.task.dateTimeWillMake,
    timeTaskReminder: state.tasks.task.timeTaskReminder,
    createdAt: state.tasks.task.createdAt,
    done: state.tasks.task.done,
    status: state.tasks.task.status,
    id: state.tasks.task.id,
    task: state.tasks.task,
    tags: state.tags.tags
})
const mapDispatchToProps = dispatch => bindActionCreators({ ...actionsTask, ...actionsTemplate, ...actionsTags }, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(TaskForm)