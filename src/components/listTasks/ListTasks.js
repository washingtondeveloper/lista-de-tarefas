import React, { Component } from 'react'
import Pagination from '../pagination/Pagination'

import Task from '../task/Task'
import Grid from '../grid/Grid'

class ListTasks extends Component {

    state = {
        pageOfItems: []
    }

    onChangePage = pageOfItems => {
        this.setState({ pageOfItems })
    }

    render() {

        const { list } = this.props

        return (
            <div>
                <div className="row">
                    {this.state.pageOfItems.map((task, i) => (
                        <Grid cols="12 6" key={i}>
                            <Task data={task} />
                        </Grid>
                    ))}

                </div>

                <Pagination items={list} onChangePage={this.onChangePage} />
            </div>
        )
    }
}

export default ListTasks