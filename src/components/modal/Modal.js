import React, { Component } from 'react'
import formSerialize from 'form-serialize'
import { Modal } from 'react-bootstrap'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import TasksForm from '../taskForm/TaskForm'
import { addTask, updateTask, cleanForm } from '../../store/actions/tasks/tasksAction'
import { showModal, hideModal } from '../../store/actions/template/templateAction'
import Utils from '../../Utils/Utils'
import TagForm from '../tagForm/TagForm';

class TaskModal extends Component {

  createOrUpdate = event => {
    event.preventDefault()
    const values = formSerialize(event.target, { hash: true })

    const task = Utils.setStatusInTask(values)

    task['tags'] = this.props.tags

    if (!task.id)
      this.props.addTask(task)
    else {
      this.props.updateTask(task)
    }

  }

  render() {
    return (
      <Modal
        show={this.props.modal}
        onHide={() => {
          this.props.hideModal()
          this.props.cleanForm()
        }}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered>
        <Modal.Header closeButton>
          <Modal.Title>{this.props.typeForm.title}</Modal.Title>
        </Modal.Header>

        <Modal.Body>
          {this.props.typeForm.type === 'task' ?
            <TasksForm
              handleSubmit={this.createOrUpdate} /> :
            <TagForm />
          }
        </Modal.Body>

      </Modal>
    )
  }
}


const mapDispatchToProps = dispatch => bindActionCreators({ addTask, updateTask, showModal, hideModal, cleanForm }, dispatch)
const mapStateToProps = state => ({ tags: state.tasks.task.tags, modal: state.template.modal, typeForm: state.template.typeForm })

export default connect(mapStateToProps, mapDispatchToProps)(TaskModal)