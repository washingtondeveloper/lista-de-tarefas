import Color from './Color'
import ColorSuccess from './ColorSuccess'
import ColorDanger from './ColorDanger'
import ColorPrimary from './ColorPrimary'
import ColorWarning from './ColorWarning'

describe('Color Functions', () => {

    let color

    beforeEach(() => {
        color = new Color()
    })

    it('Should return color Primary', () => {
        const expectColor = color.setStrategy(ColorPrimary).getColor()

        expect(expectColor).toEqual('primary')
    })

    it('Should return color Danger', () => {
        const expectColor = color.setStrategy(ColorDanger).getColor()

        expect(expectColor).toEqual('danger')
    })

    it('Should return color Success', () => {
        const expectColor = color.setStrategy(ColorSuccess).getColor()

        expect(expectColor).toEqual('success')
    })

    it('Should return color Waning', () => {
        const expectColor = color.setStrategy(ColorWarning).getColor()

        expect(expectColor).toEqual('warning')
    })
})