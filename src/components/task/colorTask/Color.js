const Color = function() {
    this.strategy = ''
}

Color.prototype = {
    setStrategy: function(strategy) {
        this.strategy = strategy
        return this
    },

    getColor: function() {
        return this.strategy.getColor()
    }
}

export default Color