import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import Utils from '../../Utils/Utils'

import Color from './colorTask/Color'
import ColorPrimary from './colorTask/ColorPrimary'
import ColorDanger from './colorTask/ColorDanger'
import ColorWarning from './colorTask/ColorWarning'
import ColorSuccess from './colorTask/ColorSuccess'
import { removeTask, getTaskById } from '../../store/actions/tasks/tasksAction'
import { showModal } from '../../store/actions/template/templateAction'
import Grid from '../grid/Grid'

class Task extends Component {

    constructor(props) {
        super(props)

        this.dateNow = new Date()
    }

    verifyColor = task => {

        const color = new Color()

        if (Utils.isTaskDone(task))
            return color.setStrategy(ColorSuccess).getColor()

        if (Utils.isTaskToday(task))
            return color.setStrategy(ColorWarning).getColor()

        if (Utils.isTaskWasYesterday(task)) {
            return color.setStrategy(ColorDanger).getColor()
        }

        return color.setStrategy(ColorPrimary).getColor()
    }

    render() {

        const date = Utils.fromStringToDate(this.props.data.dateTimeWillMake)

        const { description, tags, status, id, } = this.props.data

        return (
            <div key={id} className={`card text-white bg-${this.verifyColor(this.props.data)} mb-3`} >
                <div className="card-header">
                    Tarefa
                    <button type="button" title="Apagar" className="close" aria-label="Fechar" onClick={() => this.props.removeTask(id)}>
                        <span aria-hidden="true">&times;</span>
                    </button>{' '}
                    <button type="button" title="Editar" className="close btn-edit" onClick={() => {
                        this.props.showModal()
                        this.props.getTaskById(this.props.data.id)
                    }}>
                        <i className="fa fa-pencil-square" aria-hidden="true"></i>
                    </button>
                </div>
                <div className="card-body">

                    <div className="row">
                        <Grid cols="4 6">
                            <h4 className="card-title">{status}</h4>
                            <h6 className="card-text">{date.toLocaleDateString('pt-BR')}</h6>
                            <p className="card-text">{description}</p>
                        </Grid>
                        <Grid cols="4 6">
                            <h4 className="card-title">Tags</h4>
                            {tags && tags.map((tag, i) => <span key={i} className="badge badge-dark m-2">{tag.text}</span>)}
                            {!tags && <p className="card-text">Sem tags</p>}
                        </Grid>
                    </div>

                </div>
            </div>
        )
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({ removeTask, showModal, getTaskById }, dispatch)

export default connect(null, mapDispatchToProps)(Task)