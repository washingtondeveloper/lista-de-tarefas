import React from 'react';

import './ContentHeader.scss';

export default (props) => (
	<h1 className="t-content-header">
		{props.title} <small>{props.small}</small>
	</h1>
);
