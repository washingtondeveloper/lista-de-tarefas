import axios from 'axios'
import { API_URL } from './api'

export const tags = () =>  axios.get(`${API_URL}/tags`)
export const addtag = tag => axios.post(`${API_URL}/tags`, tag) 
export const removeTagService = id => axios.delete(`${API_URL}/tags/${id}`)