import axios from 'axios'
import { API_URL } from './api'

export const getAll = () =>  axios.get(`${API_URL}/tasks`)
export const remove = id => axios.delete(`${API_URL}/tasks/${id}`)
export const add = task => axios.post(`${API_URL}/tasks`, task)
export const getTask = id => axios.get(`${API_URL}/tasks/${id}`)
export const update = values => axios.put(`${API_URL}/tasks/${values.id}`, values)
