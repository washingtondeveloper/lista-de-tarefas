import React from 'react';
import { Route } from 'react-router-dom';

import Dashboard from '../components/template/containers/dashboard/Dashboard';
import Tasks from '../components/template/containers/tasks/Tasks';
import Search from '../components/template/containers/search/Search';
import ListTags from '../components/template/containers/listTags/ListTags';

export default () => (
	<div>
		<Route exact path="/" component={Dashboard} />
		<Route path="/tasks" component={Tasks} />
		<Route path="/search" component={Search} />
		<Route path="/tags" component={ListTags} />
	</div>
);
