import moment from 'moment'

const pattern = 'DD/MM/YYYY HH:mm'
const status = [
    'VENCE HOJE',
    'ESTA EM TEMPO',
    'CONCLUIDO',
    'PENDENTE'
]

const VENCE_HOJE = 0
const ESTA_EM_TEMPO = 1
const CONCLUIDO = 2
const PENDENTE = 3

const dateNow = new Date()

export default class Utils {

    static _isEqualYear(date) {
        return date.getFullYear() === dateNow.getFullYear()
    }

    static _isEqualMonth(date) {
        return date.getMonth() === dateNow.getMonth()
    }

    static _isEqualDate(date) {
        return date.getDate() === dateNow.getDate()
    }

    static fromStringToDate(string) {
        return moment(string, pattern).toDate()
    }

    static isTaskToday(task) {
        const date = Utils.fromStringToDate(task.dateTimeWillMake)
        return Utils._isEqualDate(date) &&
               Utils._isEqualMonth(date) &&
               Utils._isEqualYear(date)
    }

    static isTasksOfMonth(task) {
        const date = Utils.fromStringToDate(task.dateTimeWillMake)
        return Utils._isEqualMonth(date) && Utils._isEqualYear(date)
    }

    static isTaskDone(task) {
        if (typeof task.done == 'string')
            return JSON.parse(task.done)
        else
            return task.done
    }

    static isTaskWasYesterday(task) {
        const date = Utils.fromStringToDate(task.dateTimeWillMake)
        return date.getDate() < dateNow.getDate() || date.getMonth() < dateNow.getMonth() || date.getFullYear() < dateNow.getFullYear()
    }

    static setStatusInTask(task) {
        const date = Utils.fromStringToDate(task.dateTimeWillMake)

        if (typeof task.done == 'string')
            task.done = JSON.parse(task.done)

        if (task.done) {
            task.status = status[CONCLUIDO]
        } else if (date.getDate() === dateNow.getDate()) {
            task.status = status[VENCE_HOJE]
        } else if (date < dateNow) {
            task.status = status[PENDENTE]
        } else if (date > dateNow) {
            task.status = status[ESTA_EM_TEMPO]
        }

        return task
    }
}