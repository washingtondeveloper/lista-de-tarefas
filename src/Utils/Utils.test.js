import Utils from './Utils'

describe('Utils', () => {

    it('should verify date today', () => {

        const date = {
            dateTimeWillMake: new Date()
        }

        expect(Utils.isTaskToday(date)).toBe(true)
    })

    it('should verify date of month', () => {

        const date = {
            dateTimeWillMake: new Date()
        }

        expect(Utils.isTasksOfMonth(date)).toBe(true)
    })

    it('should verify date of day was yesterday', () => {

        let myDate = new Date()
        myDate.setDate(myDate.getDate() -1);

        const date = {
            dateTimeWillMake: myDate
        }

        expect(Utils.isTaskWasYesterday(date)).toBe(true)
    })

    it('should verify date of month was yesterday', () => {

        let myDate = new Date()
        myDate.setMonth(myDate.getMonth() - 1)

        const date = {
            dateTimeWillMake: myDate
        }

        expect(Utils.isTaskWasYesterday(date)).toBe(true)
    })

    it('should verify date of Year was yesterday', () => {

        let myDate = new Date()
        myDate.setFullYear(myDate.getFullYear() - 1)

        const date = {
            dateTimeWillMake: myDate
        }

        expect(Utils.isTaskWasYesterday(date)).toBe(true)
    })
})