import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'

import App from './components/App';
import store from './store'

document.addEventListener('DOMContentLoaded', function () {

	//Se não tiver suporte a Notification manda um alert para o usuário
	if (!Notification) {
		alert('Seu Browser não suporta Notificações');
		return;
	}

	//Se não tem permissão, solicita a autorização do usuário
	if (Notification.permission !== "granted")
		Notification.requestPermission()

})

ReactDOM.render(
	<Provider store={store}>
		<BrowserRouter>
			<App />
		</BrowserRouter>
	</Provider>,
	document.getElementById('root'));
