import { combineReducers } from 'redux'
import { reducer as toastrReducer } from 'react-redux-toastr'

import tasksReducer from './tasks/taksReducer'
import templateReducer from './template/templateReducer'
import tagsReducer from './tags/tagsReducer'

export default combineReducers({
    tasks: tasksReducer,
    tags: tagsReducer,
    toastr: toastrReducer,
    template: templateReducer
})