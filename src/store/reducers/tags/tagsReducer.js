import types from '../../actions/types';
const INITIAL_STATE = { tags: [] };

export default (state = INITIAL_STATE, action) => {
	switch (action.type) {
		case types.TAGS_FETCHED:
			return {
				tags: action.payload
			};
		case types.ADD_TAG:
			return {
				tags: [ ...state.tags, action.payload ]
			};
		case types.REMOVE_TAG:
			return {
				tags: state.tags.filter((tag, index) => index !== action.payload)
			};
		case types.DRAG_TAG:
			const tags = [ ...state.tags ];
			const newTags = tags.slice();

			newTags.splice(action.currPos, 1);
			newTags.splice(action.newPos, 0, action.tag);
			return {
				tags: newTags
			};
		default:
			return state;
	}
};
