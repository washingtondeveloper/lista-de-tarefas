import types from '../../actions/types'

const INITIAL_STATE = {
    task: {
        id: '',
        description: '',
        forecastTaskDuration: null,
        dateTimeWillMake: null,
        timeTaskReminder: null,
        createdAt: new Date(),
        done: false,
        status: '',
        tags: []
    },
    list: []
}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'ADD_TAG_TASK':
            return {
                ...state,
                task: { ...state.task, tags: [...state.task.tags, action.payload] }
            }
        case 'REMOVE_TAG_TASK':
            console.log('remove')
            return {
                ...state,
                task: { ...state.task, tags: state.task.tags.filter(t => t.id !== action.payload.id ) }
            }
        case types.TASKS_FETCHED:
            return {
                ...state,
                list: action.payload
            }
        case types.CHANGE_ID:
            return {
                ...state,
                task: { ...state.task, id: action.payload }
            }
        case types.CHANGE_DESCRIPTION:
            return {
                ...state,
                task: { ...state.task, description: action.payload }
            }
        case types.CHANGE_FORECAST_TASKDURATION:
            return {
                ...state,
                task: { ...state.task, forecastTaskDuration: action.payload }
            }
        case types.CHANGE_DATE_TIME_WILL_MAKE:
            return {
                ...state,
                task: { ...state.task, dateTimeWillMake: action.payload }
            }
        case types.CHANGE_TIME_TASK_REMINDER:
            return {
                ...state,
                task: { ...state.task, timeTaskReminder: action.payload }
            }
        case types.CHANGE_CREATED_AT:
            return {
                ...state,
                task: { ...state.task, createdAt: action.payload }
            }
        case types.CHANGE_DONE:
            return {
                ...state,
                task: { ...state.task, done: action.payload }
            }
        case types.CHANGE_STATUS:
            return {
                ...state,
                task: { ...state.task, status: action.payload }
            }
        case types.CLEAN_FORM:
            return {
                ...state,
                task: INITIAL_STATE.task
            }
        case types.GET_TASK_BY_ID:
            return {
                ...state,
                task: action.payload
            }
        default:
            return state
    }
}
