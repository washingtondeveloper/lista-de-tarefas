const INITIAL_STATE = { modal: false, typeForm: {} }

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SHOW_MODAL':
            return {
                ...state,
                modal: true
            }
        case 'HIDE_MODAL':
            return {
                ...state,
                modal: false
            }
        case 'CHANGE_TYPE_FORM':
            return {
                ...state,
                typeForm: action.payload
            }
        default:
            return state
    }
}