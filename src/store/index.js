import { createStore, applyMiddleware } from 'redux'
import reduxThunk from 'redux-thunk'
import multi from 'redux-multi'

import rootReducer from './reducers'

export default createStore(rootReducer, applyMiddleware(reduxThunk, multi))