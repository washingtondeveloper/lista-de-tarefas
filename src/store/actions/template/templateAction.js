export const showModal = () => ({ type: 'SHOW_MODAL' })
export const hideModal = () => ({ type: 'HIDE_MODAL' })
export const changeForm = form => ({ type: 'CHANGE_TYPE_FORM', payload: form })
