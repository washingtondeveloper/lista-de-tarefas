import { toastr } from 'react-redux-toastr'
import { getAll, add, remove, getTask, update } from '../../../service/TasksService'
import { hideModal } from '../template/templateAction'
import types from '../types'

const tasksFetched = res => {
    return {
        type: types.TASKS_FETCHED,
        payload: res.data
    }
}

const tasksFalilure = res => {
    return {
        type: types.TASKS_FAILURE,
        payload: res.data
    }
}

export const addTag = tag => {
    return {
        type: types.ADD_TAG,
        payload: tag
    }
}

export const addTagTask = tag => {
    return  {
        type: 'ADD_TAG_TASK',
        payload: tag
    }
}

export const removeTagTask = tag => {
    return {
        type: 'REMOVE_TAG_TASK',
        payload: tag
    }
}

export const removeTag = index => {
    return {
        type: types.REMOVE_TAG,
        payload: index
    }
}

export const dragTag = (tag, currPos, newPos) => {
    return {
        type: types.DRAG_TAG,
        payload: {
            tag,
            currPos,
            newPos
        }
    }
}

export const allTasks = () => {
    return dispatch => {
        getAll()
            .then(response => {
                dispatch(tasksFetched(response))
            })
            .catch(response => dispatch(tasksFalilure(response)))
    }
}

export const addTask = values => {
    return dispatch => {
        add(values)
            .then(response => {
                toastr.success('Sucesso', 'Tarefa Criada com sucesso.')
                return dispatch([
                    allTasks(),
                    hideModal(),
                    cleanForm()
                ])
            })
            .catch(response => {
                console.log('Error ao Salvar: ' + response.error)
            })
    }
}

export const updateTask = values => {
    return dispatch => {
        update(values)
            .then(response => {
                toastr.success('Sucesso', 'Tarefa Atualizada com sucesso.')
                return dispatch([
                    allTasks(),
                    hideModal(),
                    cleanForm()
                ])
            })
            .catch(response => {
                console.log('Error ao Atualizar: ' + response.error)
            })
    }
}

export const removeTask = id => {
    return dispatch => {
        remove(id)
            .then(() => {
                toastr.warning('Removido', 'Tarefa removida com sucesso.')
                dispatch([
                    allTasks()
                ])
            })
            .catch(response => {
                toastr.error('Problema', response.error)
                dispatch({ type: 'ERROR' })
            })
    }
}
export const getTaskById = id => {
    return dispatch => {
        getTask(id)
            .then(response => {
                return dispatch({ type: 'GET_TASK_BY_ID', payload: response.data })
            })
            .catch(response => {
                throw Error(response.error)
            })
    }
}

export const changeId = value => ({ type: types.CHANGE_ID, payload: value })
export const changeDescription = value => ({ type: types.CHANGE_DESCRIPTION, payload: value })
export const changeForecastTaskDuration = value => ({ type: types.CHANGE_FORECAST_TASKDURATION, payload: value })
export const changeDateTimeWillMake = value => ({ type: types.CHANGE_DATE_TIME_WILL_MAKE, payload: value })
export const changeTimeTaskReminder = value => ({ type: types.CHANGE_TIME_TASK_REMINDER, payload: value })
export const changeCreatedAt = value => ({ type: types.CHANGE_CREATED_AT, payload: value })
export const changeDone = value => ({ type: types.CHANGE_DONE, payload: value })
export const changeStatus = value => ({ type: types.CHANGE_STATUS, payload: value })
export const cleanForm = () => ({ type: types.CLEAN_FORM })