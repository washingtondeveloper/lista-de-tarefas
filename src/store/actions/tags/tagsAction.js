import { toastr } from 'react-redux-toastr';
import types from '../types';
import { tags, addtag, removeTagService } from '../../../service/TagsService';

export const addTag = (tag) => {
	return (dispatch) => {
		addtag(tag)
			.then((res) => {
				toastr.success('Sucesso', 'Tag adicionada com Sucesso!');
				dispatch([ { type: types.ADD_TAG, payload: res.data }, allTags() ]);
			})
			.catch((erro) => console.log('>>>>', erro));
	};
};

export const removeTag = (index) => {
	return (dispatch) => {
		removeTagService(index)
			.then((res) => {
				toastr.success('Sucesso', 'Tag Removida com Sucesso!');
				dispatch([
					{
						type: types.REMOVE_TAG,
						payload: index
					},
					allTags()
				]);
			})
			.catch((erro) => console.log('Não foi possivel apgar a tag', erro));
	};
};

export const dragTag = (tag, currPos, newPos) => {
	return {
		type: types.DRAG_TAG,
		payload: {
			tag,
			currPos,
			newPos
		}
	};
};

export const allTags = () => {
	return (dispatch) => {
		tags()
			.then((res) => dispatch({ type: types.TAGS_FETCHED, payload: res.data }))
			.catch((erro) => console.log(erro));
	};
};
