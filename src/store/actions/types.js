export default {
    TASKS_FETCHED: 'TASKS_FETCHED',
    TASKS_FAILURE: 'TASKS_FAILURE',
    CHANGE_DESCRIPTION: 'CHANGE_DESCRIPTION',
    CHANGE_FORECAST_TASKDURATION: 'CHANGE_FORECAST_TASKDURATION',
    CHANGE_DATE_TIME_WILL_MAKE: 'CHANGE_DATE_TIME_WILL_MAKE',
    CHANGE_TIME_TASK_REMINDER: 'CHANGE_TIME_TASK_REMINDER',
    CHANGE_CREATED_AT: 'CHANGE_CREATED_AT',
    CHANGE_DONE: 'CHANGE_DONE',
    CHANGE_STATUS: 'CHANGE_STATUS',
    CLEAN_FORM: 'CLEAN_FORM',
    CHANGE_ID: 'CHANGE_ID',
    GET_TASK_BY_ID: 'GET_TASK_BY_ID',
    ADD_TAG: 'ADD_TAG',
    REMOVE_TAG: 'REMOVE_TAG',
    DRAG_TAG: 'DRAG_TAG',
    TAGS_FETCHED: 'TAGS_FETCHED'
}